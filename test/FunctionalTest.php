<?php

use PHPUnit\Framework\TestCase;
use Behat\Mink\Element\DocumentElement;
use Behat\Mink\Element\NodeElement;

class FunctionalTests extends TestCase {

    protected $baseURL = "http://localhost:8080/index.php";
    protected $session;

    protected function setup(){
        $driver = new \Behat\Mink\Driver\GoutteDriver();
        $this->session = new \Behat\Mink\Session($driver);
        $this->session->start();
    }


    public function testCanGetPage() {
        $this->session->visit($this->baseURL);
        $page = $this->session->getPage();
    
        $this->assertInstanceOf(
            NodeElement::Class,
            $page->find('css', 'form'),
            'Form tag should exist'
          );

        $this->assertEquals('Username', $page->find('css', 'label')->getText(),
        'Label showing wrong text?');
    }

    public function testCheckNotLoggedIn(){
        $this->session->visit($this->baseURL);
        $page = $this->session->getPage();

        //if we were logged in, there would be no input with type text
        $this->assertInstanceOf(
            NodeElement::Class,
            $page->find('css', 'input[type="text"]'),
            'input type="text" tag should exist'
          );
    }

    public function testLogin(){
        $this->session->visit($this->baseURL);
        $page = $this->session->getPage();

        $form = $page->find('css', 'form');
        if($form != null){
            $inputUsername = $form->find('css', 'input[id="username"]');
            $inputPassword = $form->find('css', 'input[id="password"]');

            if($inputUsername == null){
                $this2->assertTrue(false, 'Input field: username not found');
            }else if($inputPassword == null){
                $this2->assertTrue(false, 'Input field: password not found');
            }else{

                $inputUsername->setValue('user');
                $inputPassword->setValue('pwd');
                $form->submit();
            }
        }
        $this->session->visit($this->baseURL);
        $page = $this->session->getPage();

        $this->assertInstanceOf(
                           NodeElement::Class,
                           $page->find('css', 'input[id="code"]'),
                           'there should be a input with id code on the second page'
                         );
        $this->assertEquals('Verification code', $page->find('css', 'label')->getText(),
                        'Wrong text in label after refreshing');

        $paragraph = $page->find('css', 'p')->getText();
        $trim1 = str_replace("(Pssst, the code is: ", "", $paragraph);
        $trim2 = str_replace(")", "", $trim1);

        $form = $page->find('css', 'form');
        if($form != null){
            $inputCode = $form->find('css', 'input[id="code"]');
            
            if($inputCode == null){
                $this2->assertTrue(false, 'Input field: code not found');
            }else{
                $inputCode->setValue($trim2);
                $form->submit();
            }
        
        }

        $this->session->visit($this->baseURL);
        $page = $this->session->getPage();

        $this->assertInstanceOf(
            NodeElement::Class,
            $page->find('css', 'form[method="get"]'),
            'there should be a form with method get on the last page'
          );
    }

    public function testLogout(){

        $this->session->visit($this->baseURL);
        $page = $this->session->getPage();

        $form = $page->find('css', 'form');
        
        $inputUsername = $form->find('css', 'input[id="username"]');
        $inputPassword = $form->find('css', 'input[id="password"]');

        $inputUsername->setValue('user');
        $inputPassword->setValue('pwd');

        $form->submit();

        $paragraph = $page->find('css', 'p')->getText();
        $trim1 = str_replace("(Pssst, the code is: ", "", $paragraph);
        $trim2 = str_replace(")", "", $trim1);

        $form = $page->find('css', 'form');
        
        $inputCode = $form->find('css', 'input[id="code"]');
            
        $inputCode->setValue($trim2);
        $form->submit();

        $this->assertInstanceOf(
            NodeElement::Class,
            $page->find('css', 'form[method="get"]'),
            'there should be a form with method get on the last page'
          );

        $form = $page->find('css', 'form');
        $form->submit();

        $this->session->visit($this->baseURL);
        $page = $this->session->getPage();

        $this->assertEquals('Username', $page->find('css', 'label')->getText(),
        'Label showing wrong text?');
    }

}