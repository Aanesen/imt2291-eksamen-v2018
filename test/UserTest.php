<?php
require_once "./vendor/autoload.php";
require_once "./twoFactorAuthentication/class/User.php";

use PHPUnit\Framework\TestCase;

/**
 * Class UserTest
 *
 * Uten noen sessjonsvariable skal brukeren ikke bli logget inn når et objekt av User klassen opprettes.
 * Dersom riktig sessjonsvariabel er satt så skal brukeren automatisk bli logget inn når et objekt av User klassen opprettes.
 * Når $_POST inneholder brukernavn og passord når User objektet blir opprettet så blir det generert en kode, og loginForm() metoden i klassen vil returnere en form for å fylle inn verifiseringskoden.
 * Når $_POST inneholder riktig verifiseringskode (som bestemt av tilsvarende verifiseringskode i $_SESSION) når User objektet blir opprettet så blir sessjonsvariabelen for å indikere innlogget bruker satt.
 * Når $_GET inneholder parameteren for å logge ut og $_SESSION variabelen indikerer pålogget bruker når User objektet blir opprettet så vil sessjonsvariabelen bli fjernet og ingen bruker er registrert som pålogget.
 */


final class UserTest extends TestCase{

    public function setup(){
        @session_start();
    }

    public function tearDown(){
        @session_destroy();
    }

    public function testLogin(){
        //1
        //destroys session so there won't be any session variable
        @session_destroy();
        $user = new User();

        $this->assertFalse($user->isLoggedIn());
    }

    public function testSessionLogin(){
        //loginBackdoor will set the backdoor session key
        User::loginBackdoor();
        //user should then automagically be logged in upon creation
        $user = new User();
        //assert true
        $this->assertTrue($user->isLoggedIn());
    }

    public function testUserPwdLogin(){
        //Set POST variables username and password to user and pwd
        User::userPwdBackdoor();
        //upon creation there should be a $_SESSION variable with key 'code' set
        $user = new User();
        //assert true
        $this->assertTrue($user->sessionCodeExists());
    }

    public function testVerificationCode(){
        //Set Session and Post code to what we want
        User::setSessionAndPostCode();
        //when user is created, user should be logged in and have logged in session variable
        $user = new User();
        //assert true
        $this->assertTrue($user->sessionLoggedInExists());
    }

    public function testGetLogout(){
        //set $_GET logout variable
        User::setGetLogout();
        //set $_SESSION loggedIn variable
        User::loginBackdoor();
        //when creating user, it should log out automatically
        $user = new User();
        //assert that the user is logged out
        $this->assertFalse($user->isLoggedIn());
    }
}