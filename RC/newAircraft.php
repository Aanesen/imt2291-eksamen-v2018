<?php
require_once '../vendor/autoload.php';
require_once "./DB.php";

$db = DB::getDBConnection();

$loader = new Twig_Loader_Filesystem('./');
$twig = new Twig_Environment($loader, array(
   // 'cache' => '/path/to/compilation_cache',
));

$melding = "";


if(isset($_POST['navn'])){
    $fpv = 0;
    $camera = 0;

    if(isset($_POST['fpv'])){
        $fpv = 1;
    }
    if(isset($_POST['camera'])){
        $camera = 1;
    }

    if(DB::addAircraft($db, $_POST['navn'], $fpv, $camera)){
        $melding = "Nytt fartøy er lagt til.";
    }else{
        $melding = "Noe gikk galt!";
    }
}


echo $twig->render('newAircraftView.html', array(
    'melding' => $melding,
));