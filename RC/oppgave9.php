<?php
require_once '../vendor/autoload.php';
require_once "./DB.php";

$db = DB::getDBConnection();

$loader = new Twig_Loader_Filesystem('./');
$twig = new Twig_Environment($loader, array(
    // 'cache' => '/path/to/compilation_cache',
));

$melding = "";

$batteries = DB::getBatteries($db);
$aircrafts = DB::getAircrafts($db);
$cells = DB::getCells($db);




echo $twig->render('oppgave9.html', array(
    'melding' => $melding,
    'dato' => date("Y-m-d"),
    'batteries' => $batteries,
    'aircrafts' => $aircrafts,
    'cells' => $cells,
));