<?php
require_once '../vendor/autoload.php';
require_once "./DB.php";

$db = DB::getDBConnection();

$aircrafts = DB::getAircrafts($db);
$images = DB::getImages($db);

foreach ($images as &$img){
    $img['media'] = base64_encode($img['media']);
}

header('Content-Type: application/json');
header('Access-Control-Allow-Origin: http://127.0.0.1:8081');  
header('Access-Control-Allow-Credentials: true');  

echo json_encode(array(
    'aircrafts' => $aircrafts,
    'images' => $images,
));