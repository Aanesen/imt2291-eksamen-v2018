<?php
require_once '../vendor/autoload.php';
require_once "./DB.php";

$db = DB::getDBConnection();

$loader = new Twig_Loader_Filesystem('./');
$twig = new Twig_Environment($loader, array(
    // 'cache' => '/path/to/compilation_cache',
));

$melding = "";

$batteries = DB::getBatteries($db);
$aircrafts = DB::getAircrafts($db);

if(isset($_POST['batteripakke']) && isset($_POST['fartoy']) && isset($_POST['flytid']) && isset($_POST['kapasitet']) && isset($_POST['dato'])){
    if(DB::addBatteryStatus($db, $_POST['fartoy'], $_POST['batteripakke'], $_POST['flytid'], $_POST['kapasitet'], $_POST['dato'])){
        $melding = "Batteristatus registrert.";
    }else{
        $melding = "Noe gikk galt!";
    }
}


echo $twig->render('batteryStatusView.html', array(
    'melding' => $melding,
    'dato' => date("Y-m-d"),
    'batteries' => $batteries,
    'aircrafts' => $aircrafts,
));