<?php
require_once '../vendor/autoload.php';
require_once "./DB.php";
require_once "./Help.php";

$db = DB::getDBConnection();

$loader = new Twig_Loader_Filesystem('./');
$twig = new Twig_Environment($loader, array(
    // 'cache' => '/path/to/compilation_cache',
));

//this variable will only be set if someone uploads a file.. maybe not the best way to do it
if(isset($_SERVER['HTTP_X_ORIGINALFILENAME'])) {
//Code from https://bitbucket.org/okolloen/imt2291-v2018/src/master/javascript_forelesning1/file_upload_w_progressbar/XMLHttpRequestReciveFile.php
/////////////////////////////////////////////////////////////////////
    $fname = $_SERVER['HTTP_X_ORIGINALFILENAME'];       // Get extra parameters
    $fsize = $_SERVER['HTTP_X_ORIGINALFILESIZE'];
    $mimetype = $_SERVER['HTTP_X_ORIGINALMIMETYPE'];
    $aircraftID = $_SERVER['HTTP_X_ORIGINALID'];

    $handle = fopen('php://input', 'r');                // Read the file from stdin
    $output = fopen('./images/' . $fname, 'w');
    $contents = '';

    while (!feof($handle)) {                            // Read in blocks of 8 KB (no file size limit)
        $contents = fread($handle, 8192);
        fwrite($output, $contents);
    }
    fclose($handle);
    fclose($output);

/////////////////////////////////////////////////////////////////////

    $image = file_get_contents('./images/' . $fname);

    $thumbnail = Help::scaleThumbnail($image);

    DB::addImage($db, $thumbnail, $mimetype, $fname, $aircraftID, date('Y-m-d'));
}

$melding = "";
$aircrafts = DB::getAircrafts($db);
$images = DB::getImages($db);

foreach ($images as &$img){
    $img['media'] = base64_encode($img['media']);
}

echo $twig->render('oppgave4View.html', array(
    'melding' => $melding,
    'aircrafts' => $aircrafts,
    'images' => $images,
));