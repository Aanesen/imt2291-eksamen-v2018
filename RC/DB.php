<?php

/*
 * DB class copied in and altered from Project 1 (Urgewww)
 * https://bitbucket.org/Aanesen/urgewww/src/master/classes/DB.php
 */

class DB {
    private static $db=null;
    private $dsn = 'mysql:dbname=imt2291_eksamen2018;host=127.0.0.1;charset=utf8mb4';
    private $user = 'root';
    private $password = '';
    private $dbh = null;

    /**
     * DB constructor.
     */
    private function __construct() {
        try {
            $this->dbh = new PDO($this->dsn, $this->user, $this->password);
            $this->dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); // @debug magic

        } catch (PDOException $e) {
            // NOTE IKKE BRUK DETTE I PRODUKSJON
            echo 'Connection failed: ' . $e->getMessage();
        }
    }

    /**
     * @function getDBConnection
     * @return mixed
     */
    public static function getDBConnection() {
        if (DB::$db==null) {
            DB::$db = new self();
        }
        return DB::$db->dbh;
    }

    /**
     * @function addBattery
     * @brief Inserts new battery into DB
     * @param $db - PDO connection object
     * @param $cells - how many cells om the battery
     * @param $capacity - how much charge the battery can hold
     * @param $maxDischarge - how much power the battery can deliver
     * @param $purchaseDate - when the battery is bought
     * @return bool true|false
     */
    public static function addBattery($db, $id, $cells, $capacity, $maxDischarge, $purchaseDate) {
        try{
            $sql = "INSERT INTO batteries (id, cells, capacity, maxDischarge, purchaseDate) VALUES (?, ?, ?, ?, ?)";
            $stmt = $db->prepare($sql);

            $param = array($id, $cells, $capacity, $maxDischarge, $purchaseDate);
            $stmt->execute($param);

            if ($stmt->rowCount() !== 1) {
                return false;
            }
        }catch (PDOException $e){
            return false;
        }

        return true;
    }

    /**
     * @function addAircraft
     * @brief Inserts new aircraft into DB
     * @param $db - PDO connection object
     * @param $name - name of the aircraft
     * @param $fpv - if the aircraft has first person view
     * @param $camera - if the aircraft has a camera
     * @return bool true|false
     */
    public static function addAircraft($db, $name, $fpv, $camera) {
        try{
            $sql = "INSERT INTO aircrafts (name, fpv, camera) VALUES (?, ?, ?)";
            $stmt = $db->prepare($sql);

            $param = array($name, $fpv, $camera);
            $stmt->execute($param);

            if ($stmt->rowCount() !== 1) {
                return false;
            }
        }catch (PDOException $e){
            return false;
        }

        return true;
    }

    /**
     * @function addBatteryStatus
     * @brief Inserts new batterystatus into DB
     * @param $db - PDO connection object
     * @param $craftID - ID of aircraft used
     * @param $batteryID - ID of battery used
     * @param $flighttime - how long flight
     * @param $capacityRemaining - how much capacity is left in the battery
     * @param $flightDate - date of flight
     * @return bool true|false
     */
    public static function addBatteryStatus($db, $craftID, $batteryID, $flighttime, $capacityRemaining, $flightDate) {
        try{
            $sql = "INSERT INTO batterystatus (craftId, batteryId, flightTime, capacityRemaining, flightDate) VALUES (?, ?, ?, ?, ?)";
            $stmt = $db->prepare($sql);

            $param = array($craftID, $batteryID, $flighttime, $capacityRemaining, $flightDate);
            $stmt->execute($param);

            if ($stmt->rowCount() !== 1) {
                return false;
            }
        }catch (PDOException $e){
            return false;
        }

        return true;
    }

    /**
     * @function addImage
     * @brief Inserts new aircraft into DB
     * @param $db - PDO connection object
     * @param $media - BLOB of image
     * @param $mimeType - imagetype
     * @param $filename - original filename
     * @param $craftId - aircraftID
     * @param $dateAdded - date added
     * @return bool true|false
     */
    public static function addImage($db, $media, $mimeType, $filename, $craftId, $dateAdded) {
        try{
            $sql = "INSERT INTO aircraftimages (media, mimeType, filename, craftId, dateAdded) VALUES (?, ?, ?, ?, ?)";
            $stmt = $db->prepare($sql);

            $param = array($media, $mimeType, $filename, $craftId, $dateAdded);
            $stmt->execute($param);

            if ($stmt->rowCount() !== 1) {
                return false;
            }
        }catch (PDOException $e){
            return false;
        }

        return true;
    }

    /**
     * @function getBatteries
     * @brief Retrieves the battery
     * @param $db
     * @return array|null
     */
    public static function getBatteries($db){
        try{
            //SQL Injection SAFE query method:
            $query = "SELECT id, cells, capacity, maxDischarge, purchaseDate FROM batteries ORDER BY id";
            $param = array();
            $stmt = $db->prepare($query);
            $stmt->execute($param);

            if ($stmt->rowCount()>0) {
                return $stmt->fetchAll(PDO::FETCH_ASSOC);
            }
        }catch(PDOException $ex){
            echo "Something went wrong".$ex; //Error message
        }
        return null;
    }

    /**
     * @function getCells
     * @brief Retrieves the different values of cells
     * @param $db
     * @return array|null
     */
    public static function getCells($db){
        try{
            //SQL Injection SAFE query method:
            $query = "SELECT DISTINCT(cells) FROM batteries ORDER BY id";
            $param = array();
            $stmt = $db->prepare($query);
            $stmt->execute($param);

            if ($stmt->rowCount()>0) {
                return $stmt->fetchAll(PDO::FETCH_ASSOC);
            }
        }catch(PDOException $ex){
            echo "Something went wrong".$ex; //Error message
        }
        return null;
    }

    /**
     * @function getAircrafts
     * @brief Retrieves the aircrafts
     * @param $db
     * @return array|null
     */
    public static function getAircrafts($db){
        try{
            //SQL Injection SAFE query method:
            $query = "SELECT id, name, fpv, camera FROM aircrafts ORDER BY id";
            $param = array();
            $stmt = $db->prepare($query);
            $stmt->execute($param);

            if ($stmt->rowCount()>0) {
                return $stmt->fetchAll(PDO::FETCH_ASSOC);
            }
        }catch(PDOException $ex){
            echo "Something went wrong".$ex; //Error message
        }
        return null;
    }

    /**
     * @function getImages
     * @brief Retrieves the images from db
     * @param $db
     * @return array|null
     */
    public static function getImages($db){
        try{
            //SQL Injection SAFE query method:
            $query = "SELECT media, mimeType, filename, craftID, dateAdded FROM aircraftimages ORDER BY id";
            $param = array();
            $stmt = $db->prepare($query);
            $stmt->execute($param);

            if ($stmt->rowCount()>0) {
                return $stmt->fetchAll(PDO::FETCH_ASSOC);
            }
        }catch(PDOException $ex){
            echo "Something went wrong".$ex; //Error message
        }
        return null;
    }

}