

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Registrer nytt batteri</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="main.css" />
    <script src="main.js"></script>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
</head>
<body>
    <h1>Registrer nytt batteri</h1>
    <br>
    <?php
    require_once "./DB.php";

    $db = DB::getDBConnection();

    if(isset($_POST['id']) && isset($_POST['celler']) && isset($_POST['kapasitet']) && isset($_POST['rating']) && isset($_POST['date'])){
        if(DB::addBattery($db, $_POST['id'], $_POST['celler'], $_POST['kapasitet'], $_POST['rating'], $_POST['date'])){
            echo '<h2>Nytt batteri er lagt til.</h2>';
        }else{
            echo '<h2>Noe gikk galt!</h2>';
        }
    }
    ?>
    <br>
    <form class="" action="./newBattery.php" method="post">
        <div class="row col-md-3">
            <div class="col">
                <label for="id" style="">Id:</label>
            </div>
            <div class="col">
                <input id="id" name="id" type="number" min="1" max="1000" class="form-control">
            </div>
        </div>
        <div class="row col-md-3">
            <div class="col">
                <label for="celler" style="">Celler</label>
            </div>
            <div class="col">
                <input id="celler" name="celler" type="number" min="1" max="24" class="form-control">
            </div>
        </div>
        <div class="row col-md-3">
            <div class="col">
                <label for="kapasitet" style="">Kapasitet (mAh)</label>
            </div>
            <div class="col">
                <input id="kapasitet" name="kapasitet" type="number" min="50" max="20000" class="form-control">
            </div>
        </div>
        <div class="row col-md-3">
            <div class="col">
                <label for="rating" style="">C-rating</label>
            </div>
            <div class="col">
                <input id="rating" name="rating" type="number" min="1" max="200" class="form-control">
            </div>
        </div>
        <div class="row col-md-3">
            <div class="col">
                <label for="date">Purchase date</label>
            </div>
            <div class="col">
                <input id="date" name="date" type="date" class="form-control">
            </div>
        </div>
        <div class="col-md-1">
            <input type="submit" value="Lagre informasjon">
        </div>
    </form>
</body>
</html>