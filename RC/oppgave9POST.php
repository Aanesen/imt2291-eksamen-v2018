<?php
require_once '../vendor/autoload.php';
require_once "./DB.php";

$db = DB::getDBConnection();

if(isset($_POST['batteripakke']) && isset($_POST['fartoy']) && isset($_POST['flytid']) && isset($_POST['kapasitet']) && isset($_POST['dato'])){
    if(DB::addBatteryStatus($db, $_POST['fartoy'], $_POST['batteripakke'], $_POST['flytid'], $_POST['kapasitet'], $_POST['dato'])){
        echo "Batteristatus registrert.";
    }else{
        echo "Noe gikk galt!";
    }
}