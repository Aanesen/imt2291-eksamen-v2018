<?php

class Help
{
    /**
     * @note stolen from https://stackoverflow.com/a/23173626
     */
    public static function scaleThumbnail($_img) {

        $max_width = 200;
        $max_height = 200;
        $img = imagecreatefromstring($_img);

        list($source_image_width, $source_image_height) = getimagesizefromstring($_img);

        $source_gd_image = imagecreatefromstring($_img);

        if ($source_gd_image === false) {
            return false;
        }
        $source_aspect_ratio = $source_image_width / $source_image_height;
        $thumbnail_aspect_ratio = $max_width / $max_height;
        if ($source_image_width <= $max_width && $source_image_height <= $max_height) {
            $thumbnail_image_width = $source_image_width;
            $thumbnail_image_height = $source_image_height;
        } elseif ($thumbnail_aspect_ratio > $source_aspect_ratio) {
            $thumbnail_image_width = (int) ($max_height * $source_aspect_ratio);
            $thumbnail_image_height = $max_height;
        } else {
            $thumbnail_image_width = $max_width;
            $thumbnail_image_height = (int) ($max_width / $source_aspect_ratio);
        }
        $thumbnail_gd_image = imagecreatetruecolor($thumbnail_image_width, $thumbnail_image_height);
        imagecopyresampled($thumbnail_gd_image, $source_gd_image, 0, 0, 0, 0, $thumbnail_image_width, $thumbnail_image_height, $source_image_width, $source_image_height);

        ob_start();                         // flush/start buffer
        imagepng($thumbnail_gd_image, NULL,9);          // Write image to buffer
        $scaledImage = ob_get_contents();   // Get contents of buffer
        ob_end_clean();                     // Clear buffer

        imagedestroy($source_gd_image);
        imagedestroy($thumbnail_gd_image);

        return $scaledImage;
    }
}