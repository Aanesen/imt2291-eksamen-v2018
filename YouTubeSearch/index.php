<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Page Title</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style media="screen">
        #items {
            display: inline-block;
            width: 300px;
            margin: 5px;
        }
        #overlay {
            position: fixed;
            display: none;
            width: 100%;
            height: 100%;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: rgba(0,0,0,0.5);
            z-index: 2;
            cursor: pointer;
        }
        #video-placeholder {
            margin-left: 5%;
            margin-top: 5%;
            width: 90%;
            height: 90%;
        }
    </style>
</head>
<body id="body">
<div id="overlay" onclick="off()"> <!-- off() makes the overlay go away :) -->
    <div id="video-placeholder"></div>
</div>

<script src="https://www.youtube.com/iframe_api"></script>
    <script> 
        //youtube iframe api thing
        var player;

        function onYouTubeIframeAPIReady() {
            player = new YT.Player('video-placeholder', {
                videoId: 'Xa0Q0J5tOP0', //default video
                playerVars: {
                    color: 'white'
                },
                events: {
                    onReady: initialize
                }
            });
        }
        //youtube thing end


        var body = document.getElementById('body');
        var data = null;
        fetch('./search.php')
        .then(function(response) {
            return response.json();
        })
        .then(function(myJson) {
            console.log(myJson);
            var data = myJson.items;
            var arrayLength = data.length;
            for (var i = 0; i < arrayLength; i++) {
                if(data[i].id.kind == "youtube#video"){                 //I go through all the items from the json
                    var div = document.createElement('div');            //and add the text/images to elements
                    var title = document.createElement('h1');           //then i put all the elements together
                    var image = document.createElement('img');          //before i add an eventlistener to handle clicks
                    var desc = document.createElement('p');             //i also pass it a parameter to change the current
                    title.innerHTML = data[i].snippet.title;            //playing video.
                    image.src = data[i].snippet.thumbnails.medium.url;
                    desc.innerHTML = data[i].snippet.description;
                    div.id = "items";
                    div.appendChild(title);
                    div.appendChild(image);
                    div.appendChild(desc);
                    body.appendChild(div);
                    div.addEventListener("click", changeVideo, false);
                    div.myParam = data[i].id.videoId;
                    function changeVideo(e){
                        //turn overlay on
                        document.getElementById("overlay").style.display = "block";
                        //change video
                        player.loadVideoById(e.currentTarget.myParam, 0, "large")
                    }
                }

            }
        })
        .catch(function(error) {
            console.log(error);
        });

    
        //overlay functions taken from: https://www.w3schools.com/howto/tryit.asp?filename=tryhow_css_overlay
        function off() {
            //turns off overlay
            document.getElementById("overlay").style.display = "none";
        }

        //youtube thing
        function initialize(){
            // Update the controls on load
            updateTimerDisplay();
            updateProgressBar();

            // Clear any old interval.
            clearInterval(time_update_interval);

            // Start interval to update elapsed time display and
            // the elapsed part of the progress bar every second.
            time_update_interval = setInterval(function () {
                updateTimerDisplay();
                updateProgressBar();
            }, 1000)
        }
        
    </script>
</body>
</html>