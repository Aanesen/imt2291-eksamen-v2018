<?php

class User{

    private $loggedin = false;

    public function __construct() {

        if (isset($_POST['username']) && isset($_POST['password'])) {
          if($_POST['username'] == "user" && $_POST['password'] == 'pwd'){
              $this->setSession();
          }
          
        } else if (isset($_GET['logout'])) {
            $this->logout();

        } else if (isset($_SESSION['code']) && isset($_POST['code'])) {
            if($_SESSION['code'] == $_POST['code']){
                $this->login();
            }

        } else if (isset($_SESSION['loggedIn'])){
            $this->login();
        }
    }

    public function loginForm(){
        if($this->loggedin){
            return "<form method='get' action='./index.php'>
                    <input type='hidden' name='logout' value='true'></input>
                    <input type='submit' value='Logout'></input>
                    </form>";
        }else if(isset($_SESSION['code'])){
            return  "<form method='post' action='./index.php'>
                     <label for='code'>Verification code</label>
                     <input type='text' id='code' name='code'></input>
                     <br><br>
                     <input type='submit' value='Confirm code'></input>
                     </form>
                     <p>(Pssst, the code is: ".$_SESSION['code'].")</p>"; 
        }else{
            return "<form method='post' action='./index.php'>
                    <label for='username'>Username</label>
                    <input type='text' id='username' name='username'></input>
                    <br><br>
                    <label for='password'>Password</label>
                    <input type='password' id='password' name='password'></input>
                    <br><br>
                    <input type='submit' value='log in'></input>
                    </form>";
        }
    }

    public function logout(){
        $this->loggedin = false;
        $this->destroySession();
    }

    private function login(){
        $_SESSION['loggedIn'] = true;
        $this->loggedin = true;
    }

    public function isLoggedIn(){
        return $this->loggedin;
    }

    public function setSession(){
        //rand line from https://stackoverflow.com/a/5438857
        $_SESSION['code'] = substr(uniqid('', true), -4);
    }

    public function destroySession(){
        unset($_SESSION['code']);
        unset($_SESSION['loggedIn']);
    }

    public function sessionCodeExists(){
        return isset($_SESSION['code']);
    }

    public function sessionLoggedInExists(){
        return isset($_SESSION['loggedIn']);
    }

    public static function setSessionAndPostCode(){
        $_SESSION['code'] = substr(uniqid('', true), -4);
        $_POST['code'] = $_SESSION['code'];
    }

    public function getSessionCode(){
        return $_SESSION['code'];
    }

    public static function loginBackdoor(){
        $_SESSION['loggedIn'] = true;
    }

    public static function userPwdBackdoor(){
        $_POST['username'] = 'user';
        $_POST['password'] = 'pwd';
    }

    public static function setGetLogout(){
        $_GET['logout'] = true;
    }
}

//takk Øivind :) https://bitbucket.org/okolloen/imt2291-eksamen-v2018/issues/10/oppgave-5-bare-bruke-userphp-classen
$user = new User();